const Booking = require("../models/bookingModel");
const QR = require("../models/QR");
const util = require("util");
const nodemailer = require("nodemailer");
//InternationalPayment
var ResMsgDTO = require("../ResMsgDTO.js");
var ReqMsgDTO = require("../ReqMsgDTO.js");
var VtransactSecurity = require("../VtransactSecurity");
var HTTPPost = require("../HTTPPost.js");
var AWLMEAPI = require("../AWLMEAPI.js");
var reqMsgDTO = new ReqMsgDTO();
var resMsgDTO = new ResMsgDTO();
var transactMeAPI = new AWLMEAPI();
var vTransactSecurity = new VtransactSecurity();
var express = require("express");
var bodyParser = require("body-parser");
var app = express();
const fs = require("fs");
const ini = require("ini");
app.use(express.static(__dirname));
app.use(bodyParser.urlencoded({ extended: false }));
// const ini_array = ini.parse(fs.readFileSync("./../ClientAPI.ini", "utf-8"));
const path = require("path");
const filePath = path.join(__dirname, "..", "ClientAPI.ini");
const ini_array = ini.parse(fs.readFileSync(filePath, "utf-8"));

exports.paymentMethod = async (req, res, next) => {
  try {
    res.render("./Payment/paymentType.ejs");
  } catch (error) {
    console.log(error);
  }
};

exports.localPayment = async (req, res, next) => {
  try {
    const QRs = await QR.find();
    res.render("./Payment/localPayment.ejs", { QRs });
  } catch (error) {
    console.log(error);
  }
};
//Trasaction success
exports.transactionSuccessful = async (req, res, next) => {
  // Parse Merchant encryption to parse PG response message
  var merchantResponse = null;
  console.log("FIrst of its kind");
  console.log(req.body.merchantResponse, "merchantresponse");
  resMsgDTO = null;
  if (req.body.merchantResponse != null) {
    // Merchant Encryption Key var
    // console.log(req.body.merchantResponse);
    // var enc_key = "3699a6289277c23e8efa6ba12c3bb517";
    var enc_key = ini_array.enyckey;
    // console.error('enc_key ' + enc_key)
    // Get PG transaction response
    merchantResponse = req.body.merchantResponse;
    // Initialise object to parse response message
    //AWLMEAPI transactMeAPI = new AWLMEAPI();
    // Call method to parse PG transaction response

    // console.log(merchantResponse);
    resMsgDTO = await transactMeAPI.parseTrnResMsg(merchantResponse, enc_key);

    console.log('hi res from book', resMsgDTO);
    // var varAddField1 = resMsgDTO.getAddField1();
    // var varAddField2 = resMsgDTO.getAddField2();
    // var varAddField3 = resMsgDTO.getAddField3();
    // var varAddField4 = resMsgDTO.getAddField4();
    // var varAddField5 = resMsgDTO.getAddField5();
    // var varAddField6 = resMsgDTO.getAddField6();
    // var varAddField7 = resMsgDTO.getAddField7();
    // var varAddField8 = resMsgDTO.getAddField8();
    // var varAddField9 = resMsgDTO.getAddField9();
    // var varAddField10 = resMsgDTO.getAddField10();
    var varAuthZCode = resMsgDTO.getAuthZCode();
    var varOrderId = resMsgDTO.getOrderId();
    var varResponseCode = resMsgDTO.getResponseCode();
    var varRrn = resMsgDTO.getRrn();
    var varStatusDesc = resMsgDTO.getStatusDesc();
    var varTrnAmt = resMsgDTO.getTrnAmt();
    var varTrnRefNo = resMsgDTO.getPgMeTrnRefNo();
    var varTrnReqDate = resMsgDTO.getTrnReqDate();
   

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    const dateString = resMsgDTO.getCheckinDate; // Assuming dateString is the string representation of the date.
    const dateObject = new Date(dateString);
    // const checkin = resMsgDTO.getCheckinDate.toLocaleDateString("en-US", {
    //   weekday: "short",
    //   month: "short",
    //   day: "numeric",
    //   year: "numeric",
    // });
    let checkin;
    if (!isNaN(dateObject.getTime())) {
      checkin = dateObject.toLocaleDateString("en-US", {
        weekday: "short",
        month: "short",
        day: "numeric",
        year: "numeric",
      });

      // Now 'checkin' contains the formatted date string.
    }
    const dateString1 = resMsgDTO.getCheckoutDate; // Assuming dateString is the string representation of the date.
    const dateObject1 = new Date(dateString1);
    let checkout;
    if (!isNaN(dateObject1.getTime())) {
      checkout = dateObject1.toLocaleDateString("en-US", {
        weekday: "short",
        month: "short",
        day: "numeric",
        year: "numeric",
      });

      // Now 'checkin' contains the formatted date string.
    }
    // const checkout = resMsgDTO.getCheckoutDate.toLocaleDateString("en-US", {
    //   weekday: "short",
    //   month: "short",
    //   day: "numeric",
    //   year: "numeric",
    // });

    const checkedId = new Date(resMsgDTO.getCheckinDate);
    const checkouted = new Date(resMsgDTO.getCheckoutDate);
    const oneDay = 24 * 60 * 60 * 1000; // Number of milliseconds in a day
    const diffInDays = Math.round(
      Math.abs((checkouted.getTime() - checkedId.getTime()) / oneDay)
    );
    console.log("Email", resMsgDTO, resMsgDTO.getEmail);
    var statusCode = resMsgDTO.statusCode
    const mailOptions = {
      from: process.env.EMAIL_USER,
      to: resMsgDTO.email,
      subject:
        "You have successfully booked your stay at THE PEMA BY REALM, Thimphu",
      html: `
      <!DOCTYPE html>
      <html lang="en">
      
      <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Reset Email Button</title>
      
          <style>
              body {
                  margin: 0 !important;
                  font-family: system-ui, 'Helvetica Neue';
              }
      
              .container {
                  width: 100%;
                  max-width: 700px;
                  height: fit-content;
                  margin: auto;
                  background: #DE9D3B;
                  color: #000;
                  padding: 2rem;
                  border-radius: 6px;
              }
      
              .nav-bar {
                  width: 100%;
                  text-align: center;
              }
      
              .hotel-logo {
                  width: 100px;
                  height: 100px;
                  margin: 0 auto;
              }
      
              .reservation-data {
                  margin-top: 1.4rem;
                  text-align: justify;
              }
      
              .reservationID {
                  color: orange;
              }
      
              .footer {
                  margin-top: 1rem;
              }
      
              table,
              th,
              td {
                  border: 1px solid wheat;
                  padding: 5px;
              }
      
              table {
                  border: 1px solid wheat;
                  border-collapse: collapse;
                  width: 100%;
                  border-radius: 2px;
              }
      
              .reservationNumber {
                  text-align: center;
                  font-size: 18px;
              }
          </style>
          </style>
      </head>
      
      <body>
      
      
          <div class="container">
              <div class="nav-bar">
                  <img src="http://139.59.46.216/wp-content/uploads/2022/10/Untitled-1.png" class="hotel-logo" alt="Logo">
                  <br><br>
                  <b class="reservationNumber">RESERVATION CONFIRMATION</b>
              </div>
      
              <div class="reservation-data">
                  <p>
                      Dear ${resMsgDTO.name} & Group,
                  </p>
      
                  <p>
                      We are pleased to confirm your reservation with The Pema by Realm, Thimphu. Here is a summary of your
                      booking and information. We look forward to welcoming you and making your stay a memorable one.
                  </p>
                  <br>
                  <table>
                      <tbody>
                          <tr>
                              <td>
                                  <b>Guest Name</b>
                              </td>
                              <td>
                                  ${resMsgDTO.name}
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Email ID
                              </td>
                              <td>
                                  ${resMsgDTO.email}
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Telephone No
                              </td>
                              <td>
                                  ${resMsgDTO.contactNumber}
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Check In Date
                              </td>
                              <td>
                                  ${resMsgDTO.checkinDate} &nbsp;
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14.00
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Check Out Date
                              </td>
                              <td>
                                  ${resMsgDTO.checkoutDate} &nbsp;
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14.00
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <b>
                                      Confirmation Number
                                  </b>
                              </td>
                              <td>
                                  <b>
                                      ${resMsgDTO.orderId}
                                  </b>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <b>
                                      Reservation Status
                                  </b>
                              </td>
                              <td>
                                  <b>
                                      Confirmed
                                  </b>
                              </td>
                          </tr>
                      </tbody>
                  </table>
                  <br>
      
                  <p><b>Room and Rate Information</b></p>
                  <table>
                      <tbody>
      
                          <tr>
                              <td>
                                  Room Rate
                              </td>
                              <td>
                                  ${resMsgDTO.trnAmt}
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Room Type
                              </td>
                              <td>
                                  ${resMsgDTO.roomType}
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  No of Room Nights
                              </td>
                              <td>
                                  ${
                                    resMsgDTO.getTotalRoomTypes
                                  } Room ${diffInDays} Nights
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Pax (Adult / Child)
                              </td>
                              <td>
                                  ${resMsgDTO.adultNumber} / ${
        resMsgDTO.getChildMinorNumber + resMsgDTO.getChildNumber
      }
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Room Includes
                              </td>
                              <td>
                                  Continental Plan (Room + Breakfast + Taxes)
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Special Instructions
                              </td>
                              <td>
                                 ${resMsgDTO.specialRequirement}
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <br>
              <div class="footer">
      
                  <p>
                      For any queries/clarifications, please feel free to contact our Reservation Executive at the below
                      contact
                      details.
                  </p>
                  <table>
                      <tbody>
                          <tr>
                              <td>
                                  Reservation Executive
                              </td>
                              <td>
                                  Mr. Hem
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Contact Number
                              </td>
                              <td>
                                  <p>
                                      <b> +975-17279401 </b> (mobile / whatsapp available)<br>
                                      <b>+975-2-338888</b> (Hotel Reception Desk)
                                  </P>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <br>
              <div>
                  <p><b>Terms and Conditions</b></p>
                  <ol>
                      <li>Check in time is at 14.00 hrs and checkout time is 12 noon.</li>
                      <li>Maximum two adults and one child below the age of 12 yrs are permitted per room without an extra bed.</li>
                      <li>Rates are quoted in Ngultrum/US Dollar and are based on single/double/triple occupancy.</li>
                      <li>Cancellation received less than 14 days prior to check in will incur 100% cancellation fee.</li>
                      <li>No show or early departures will be charged in full.</li>
                      <li>Early check in and late checkout may be considered on availability, and may be subject to additional charges. </li>
                  </ol>
              </div>
      
          </div>
      
      
      </body>
      
      </html>
      `,
    };

    if(statusCode !== 'F'){
      transporter.sendMail(mailOptions, async function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log("Email sent: " + info.response);
        }
      });
    }

   
  } else {
    merchantResponse = "No response";
  }

  // res.sendFile('meTrnSuccess.html', { root: '.' })
  res.render("./booking/meTrnSuccess.ejs", {
    AuthZCode: varAuthZCode,
    OrderId: varOrderId,
    ResponseCode: varResponseCode,
    Rrn: varRrn,
    StatusDesc: varStatusDesc,
    TrnAmt: varTrnAmt / 100.0,
    TrnRefNo: varTrnRefNo,
    TrnReqDate: varTrnReqDate,
    statusCode
  });
};
//international payment
exports.internationalPayment = async (req, res, next) => {
  const bookingDataString = req.body.bookingData;

  // Split the string into lines
  const lines = bookingDataString.split("\r\n");

  // Create an object to store the key-value pairs
  const parsedBookingData = {};

  for (const line of lines) {
    // Split each line into key and value
    const [key, value] = line.split(": ");

    // Assign key and value to the parsedBookingData object
    parsedBookingData[key] = value;
  }
  //Need to change in production side
  var mid = "";
  var enckey = "";
  if(parsedBookingData.currency === '$'){
    mid = ini_array.mid
    enckey = ini_array.enyckey
    reqMsgDTO.setEnckey(enckey);
  }
  else{
    mid = ini_array.mid
    enckey = ini_array.enyckey
    reqMsgDTO.setEnckey(enckey);
  }
  // var mid = parsedBookingData.mid;
  if (mid != null) {
    // mid = parsedBookingData.mid;
    // console.log(mid);
    reqMsgDTO.setMid(mid);
  } else {
    mid = PaytmConfig.mid;
    reqMsgDTO.setMid(mid);
  }

  // Merchant Encryption key
 
  // if (parsedBookingData.encKey != null) {
  //   enckey = parsedBookingData.encKey;
  //   reqMsgDTO.setEnckey(enckey);
  // } else {
  //   enckey = PaytmConfig.enckey;
  //   reqMsgDTO.setEnckey(enckey);
  // }
  //Merchant unique order id
  var orderId = parsedBookingData.orderId;
  if (parsedBookingData.orderId != null) {
    // orderId = parsedBookingData.hdnOrderID;
    reqMsgDTO.setOrderId(orderId);
  }
  // Transaction amount in paisa format
  var amt = null;
  reqMsgDTO.setTrnAmt(parsedBookingData.trnAmt);
  // Merchant transaction currency
  reqMsgDTO.setTrnCurrency(parsedBookingData.currency);
  // Transaction remarks
  var trnRemarks = null;
  if (parsedBookingData.trnRemarks != null) {
    trnRemarks = parsedBookingData.trnRemarks;
    reqMsgDTO.setTrnRemarks(trnRemarks);
  }
  // Merchant request type S/P/R
  var meTransReqType = null;
  if (parsedBookingData.meTransReqType != null) {
    meTransReqType = parsedBookingData.meTransReqType;
    reqMsgDTO.setMeTransReqType(meTransReqType);
  }
  // Recurring period (M/W))if merchant request type is R
  var recurPeriod = null;
  if (parsedBookingData.recPeriod != null) {
    recurPeriod = parsedBookingData.recPeriod;
    reqMsgDTO.setRecurrPeriod(recurPeriod);
  }
  //Recurring day if merchant request type is R: Recurring Payment
  var recurDay = null;
  if (parsedBookingData.recDay != null) {
    recurDay = parsedBookingData.recDay;
    reqMsgDTO.setRecurrDay(recurDay);
  }
  //No of recurring if merchant request type is R
  var numberRecurring = null;
  if (parsedBookingData.noOfRec != null) {
    numberRecurring = parsedBookingData.noOfRec;
    reqMsgDTO.setNoOfRecurring(numberRecurring);
  }
  //Merchant response URL
  var ResponseUrl = null;

  if (parsedBookingData.resUrl != null) {
    ResponseUrl = parsedBookingData.resUrl;
    reqMsgDTO.setResponseUrl(ResponseUrl);
  } else {
    ResponseUrl = PaytmConfig.website;
    reqMsgDTO.setResponseUrl(ResponseUrl);
  }

  //Optional Addition fields for Merchant use
  // reqMsgDTO.setAddField1(parsedBookingData.addField1);
  reqMsgDTO.setName(parsedBookingData.name);
  //reqMsgDTO.setAddField2(parsedBookingData.addField2);
  reqMsgDTO.setEmail(parsedBookingData.email);
  // reqMsgDTO.setAddField3(parsedBookingData.addField3);
  // reqMsgDTO.setAddField4(parsedBookingData.addField4);
  // reqMsgDTO.setAddField5(parsedBookingData.addField5);
  reqMsgDTO.setContactNumber(parsedBookingData.contactNumber);
  reqMsgDTO.setSpecialRequirement(parsedBookingData.specialRequirement);
  reqMsgDTO.setRoomType(parsedBookingData.roomType);
  // reqMsgDTO.setAddField6(parsedBookingData.addField6);
  // reqMsgDTO.setAddField7(parsedBookingData.addField7);
  // reqMsgDTO.setAddField8(parsedBookingData.addField8);
  reqMsgDTO.setCheckinDate(parsedBookingData.checkinDate);
  reqMsgDTO.setCheckoutDate(parsedBookingData.checkoutDate);
  reqMsgDTO.setAdultNumber(parsedBookingData.adultNumber);
  // reqMsgDTO.setAddField9("");
  // reqMsgDTO.setAddField10("");
  // reqMsgDTO.setAddField9(parsedBookingData.addField9);
  // reqMsgDTO.setAddField10(parsedBookingData.addField10);
  reqMsgDTO.setChildMinorNumber(parsedBookingData.childMinorNumber);
  reqMsgDTO.setChildNumber(parsedBookingData.childNumber);
  reqMsgDTO.setTotalRoomTypes(parsedBookingData.totalRoomTypes);
  reqMsgDTO.setTotalRooms(parsedBookingData.totalRooms);
  reqMsgDTO.setStatusDesc("");
  // console.log('test');
  var merchantRequest = null;
  try {
    // Initialise object to generate transaction request message AWLMEAPI
    // var transactMeAPI = new AWLMEAPI();
    reqMsgDTO = transactMeAPI.generateTrnReqMsg(reqMsgDTO);
    console.log(reqMsgDTO);
    // Check status desciption for message generation
    if (reqMsgDTO.getStatusDesc() == "Success") {
      merchantRequest = reqMsgDTO.getReqMsg();
      // console.log(merchantRequest);
      // var txn_url = "https://cgt.in.worldline.com/ipg/doMEPayRequest"; // for staging
      var txn_url = ini_array.STD_PAY;
      var form_fields="";
      form_fields +=
        "<input type='hidden' name='merchantRequest'  value='" +
        merchantRequest +
        "' >";
      form_fields +=
        "<input type='hidden' name='MID' id = 'MID' value = '" + mid + "'/>";
      console.log(form_fields);
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(
        '<html><head><title>Merchant Checkout Page</title></head><body><center><h1>Please do not refresh this page...</h1></center><form method="post" action="' +
          txn_url +
          '" name="f1">' +
          form_fields +
          '</form><script type="text/javascript">document.f1.submit();</script></body></html>'
      );
      // console.log(util.inspect(res, { depth: null }));
      res.end();
    }
    // merchantRequest = reqMsgDTO.getReqMsg();
    // res.send(merchantRequest + ' Submitted Successfully!');
  } catch (ex) {
    reqMsgDTO.setStatusDesc("Error Occurred during processing" + ex);
    console.log(ex);
    // throw new Exception(ex.getMessage());
  } // If request message generate
};

exports.getAllBookings = async (req, res) => {
  try {
    const booking = await Booking.find();
    res.status(200).json({ data: booking, status: "success" });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.createBooking = async (req, res) => {
  try {
    const booking = await Booking.create(parsedBookingData);

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    const mailOptionsToCustomer = {
      from: process.env.EMAIL_USER,
      to: booking.email,
      subject:
        "You have successfully booked your stay at THE PEMA BY REALM, Thimphu",
      html: `
            <!DOCTYPE html>
            <html lang="en">
            
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Reset Email Button</title>
            
                <style>
                    body {
                        margin: 0 !important;
                        font-family: system-ui, 'Helvetica Neue';
                    }
            
                    .container {
                        width: 100%;
                        max-width: 600px;
                        height: fit-content;
                        margin: auto;
                        background: #333132;
                        color: #fff !important;
                        padding: 2rem;
                        border-radius: 6px;
                    }
            
                    .nav-bar {
                        width: 100%;
                        text-align: center;
                    }
            
                    .hotel-logo {
                        width: 100px;
                        height: 100px;
                        margin: 0 auto;
                    }
            
                    .reservation-data {
                        margin-top: 1.4rem;
                    }
            
                    .reservationID {
                        color: orange;
                    }
            
                    .footer {
                        margin-top: 2rem;
                    }
            
                    table,
                    th,
                    td {
                        border: 1px solid wheat;
                        padding: 5px;
                    }
            
                    table {
                        border: 1px solid wheat;
                        border-collapse: collapse;
                        width: 100%;
                        border-radius: 2px;
                    }
            
                    .reservationNumber {
                        text-align: center;
                        font-size: 15px;
                    }
                </style>
                </style>
            </head>
            
            <body>
            
            
                <div class="container">
                    <div class="nav-bar">
                        <img src="http://139.59.46.216/wp-content/uploads/2022/10/Untitled-1.png" class="hotel-logo" alt="Logo">
                        <br><br>
                        <b class="reservationNumber">RESERVATION NUMBER : ${booking.reservationID}</b>
                        <br><br>
                    </div>
                    <hr width="100%" color="orange">
                    <div class="reservation-data">
                        <p>
                            Booking successful for <b class="reservationID">${booking.reservationID}</b>
                        </p>
            
                        <p>
                            Final confirmation of your reservation shall be sent by email following payment verification.
            
                        </p>
            
                        <p>
                            Thank you very much.
                        </p>
                        <p>
                            Warmly,
                            The Pema by Ream Family
                        </p>
            
                    </div>
                    <div class="footer">
                        <hr width="100%" color="orange">
                        <p>
                            For any queries/clarifications, please feel free to contact our Reservation Executive at the below
                            contact
                            details.
                        </p>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        Reservation Executive
                                    </td>
                                    <td>
                                        Mr. Hem
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact Number
                                    </td>
                                    <td>
                                        <p>
                                            <b> +975-17279401 </b> (mobile / whatsapp available)<br>
                                            <b>+975-2-338888</b> (Hotel Reception Desk)
                                        </P>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            
                </div>
            
            
            </body>
            
            </html>`,
    };

    const mailOptionsToManager = {
      from: process.env.EMAIL_USER,
      to: process.env.MANAGER_EMAIL,
      subject: "New Transacion Processed",
      html: `
            <!DOCTYPE html>
            <html lang="en">
            
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Reset Email Button</title>
            
                <style>
                    body {
                        margin: 0 !important;
                        font-family: system-ui, 'Helvetica Neue';
                    }
            
                    .container {
                        width: 100%;
                        max-width: 600px;
                        height: fit-content;
                        margin: auto;
                        background: #DE9D3B;
                        color: #000;
                        padding: 2rem;
                        border-radius: 6px;
                    }
            
                    .nav-bar {
                        width: 100%;
                        text-align: center;
                    }
            
                    .hotel-logo {
                        width: 100px;
                        height: 100px;
                        margin: 0 auto;
                    }
            
                    .reservation-data {
                        margin-top: 1.4rem;
                    }
            
                    .reservationID {
                        color: orange;
                    }
            
                    .footer {
                        margin-top: 2rem;
                    }
            
                    table,
                    th,
                    td {
                        border: 1px solid wheat;
                        padding: 5px;
                    }
            
                    table {
                        border: 1px solid wheat;
                        border-collapse: collapse;
                        width: 100%;
                        border-radius: 2px;
                    }
            
                    .reservationNumber {
                        text-align: center;
                        font-size: 15px;
                    }
                </style>
                </style>
            </head>
            
            <body>
            
            
                <div class="container">
                    <div class="nav-bar">
                        <img src="http://139.59.46.216/wp-content/uploads/2022/10/Untitled-1.png" class="hotel-logo" alt="Logo">                       
                        <br><br>
                    </div>
                    <hr width="100%" color="orange">
                    <div class="reservation-data">
                        <p>A new transaction has been processed at THE PEMA BY REALM. Here are the details:</p>
                        <p><strong>Reservation ID:</strong> ${booking.reservationID}</p>
                        <p><strong>Customer Name:</strong> ${booking.name}</p>
                        <p><strong>Email:</strong> ${booking.email}</p>
                        <p><strong>Amount:</strong> ${booking.totalAmount}</p>
                        <p>Please review and ensure all necessary steps are taken to confirm and process the transaction.</p>
                        <p>Thank you</p>
                    </div>
                </div>
            </body>
            </html>`,
    };

    await transporter.sendMail(mailOptionsToCustomer);
    await transporter.sendMail(mailOptionsToManager);

    console.log(
      "Email sent successfully for reservation ID:",
      booking.reservationID
    );

    res.json({ data: booking, status: "success" });
  } catch (err) {
    console.error("Error sending email:", err);
    res.status(500).json({ error: err.message });
  }
};

exports.getBooking = async (req, res) => {
  try {
    const booking = await Booking.findById(req.params.id);
    res.json({ data: booking, status: "success" });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.updateBooking = async (req, res) => {
  try {
    const booking = await Booking.findByIdAndUpdate(
      req.params.id,
      parsedBookingData
    );
    res.json({ data: booking, status: "success" });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.deleteBooking = async (req, res) => {
  try {
    const booking = await Booking.findByIdAndDelete(req.params.id);
    res.json({ data: booking, status: "success" });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};
