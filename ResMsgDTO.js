// class ResMsgDTO {

// pgMeTrnRefNo;
// orderId;
// authNStatus;
// authZStatus;
// captureStatus;
// rrn;
// authZCode;
// responseCode;
// trnReqDate;
// statusCode;
// statusDesc;
// addField1;
// addField2;
// addField3;
// addField4;
// addField5;
// addField6;
// addField7;
// addField8;
// addField9;
// addField10;
// pgRefCancelReqId;
// trnAmt;
// refundAmt;

function ResMsgDTO() {}

ResMsgDTO.prototype.getPgMeTrnRefNo = function () {
  return this.pgMeTrnRefNo;
};

ResMsgDTO.prototype.setPgMeTrnRefNo = function (pgMeTrnRefNo) {
  this.pgMeTrnRefNo = pgMeTrnRefNo;
};

ResMsgDTO.prototype.getOrderId = function () {
  return this.orderId;
};

ResMsgDTO.prototype.setOrderId = function (orderId) {
  this.orderId = orderId;
};

ResMsgDTO.prototype.getAuthNStatus = function () {
  return this.authNStatus;
};

ResMsgDTO.prototype.setAuthNStatus = function (authNStatus) {
  this.authNStatus = authNStatus;
};

ResMsgDTO.prototype.getAuthNStatus = function () {
  return this.authNStatus;
};

ResMsgDTO.prototype.setAuthNStatus = function (authNStatus) {
  this.authNStatus = authNStatus;
};

ResMsgDTO.prototype.getAuthZStatus = function () {
  return this.authZStatus;
};

ResMsgDTO.prototype.setAuthZStatus = function (authZStatus) {
  this.authZStatus = authZStatus;
};

ResMsgDTO.prototype.getCaptureStatus = function () {
  return this.captureStatus;
};

ResMsgDTO.prototype.setCaptureStatus = function (captureStatus) {
  this.captureStatus = captureStatus;
};

ResMsgDTO.prototype.getRrn = function () {
  return this.rrn;
};

ResMsgDTO.prototype.setRrn = function (rrn) {
  this.rrn = rrn;
};

ResMsgDTO.prototype.getAuthZCode = function () {
  return this.authZCode;
};

ResMsgDTO.prototype.setAuthZCode = function (authZCode) {
  this.authZCode = authZCode;
};

ResMsgDTO.prototype.getResponseCode = function () {
  return this.responseCode;
};

ResMsgDTO.prototype.setResponseCode = function (responseCode) {
  this.responseCode = responseCode;
};

ResMsgDTO.prototype.getTrnReqDate = function () {
  return this.trnReqDate;
};

ResMsgDTO.prototype.setTrnReqDate = function (trnReqDate) {
  this.trnReqDate = trnReqDate;
};

ResMsgDTO.prototype.getStatusCode = function () {
  return this.statusCode;
};

ResMsgDTO.prototype.setStatusCode = function (statusCode) {
  this.statusCode = statusCode;
};

ResMsgDTO.prototype.getStatusDesc = function () {
  return this.statusDesc;
};

ResMsgDTO.prototype.setStatusDesc = function (statusDesc) {
  this.statusDesc = statusDesc;
};

ResMsgDTO.prototype.getPgRefCancelReqId = function () {
  return this.pgRefCancelReqId;
};

ResMsgDTO.prototype.setPgRefCancelReqId = function (pgRefCancelReqId) {
  this.pgRefCancelReqId = pgRefCancelReqId;
};

ResMsgDTO.prototype.getRefundAmt = function () {
  return this.refundAmt;
};

ResMsgDTO.prototype.setRefundAmt = function (refundAmt) {
  this.refundAmt = refundAmt;
};
//From Here, non addtional fields

ResMsgDTO.prototype.getTrnAmt = function () {
  return this.trnAmt;
};

ResMsgDTO.prototype.setTrnAmt = function (trnAmt) {
  this.trnAmt = trnAmt;
};

ResMsgDTO.prototype.getTrnCurrency = function () {
  return this.trnCurrency;
};

ResMsgDTO.prototype.setTrnCurrency = function (trnCurrency) {
  this.trnCurrency = trnCurrency;
};

ResMsgDTO.prototype.getTrnRemarks = function () {
  return this.trnRemarks;
};

ResMsgDTO.prototype.setTrnRemarks = function (trnRemarks) {
  this.trnRemarks = trnRemarks;
};

ResMsgDTO.prototype.getMeTransReqType = function () {
  return this.meTransReqType;
};

ResMsgDTO.prototype.setMeTransReqType = function (meTransReqType) {
  this.meTransReqType = meTransReqType;
};

ResMsgDTO.prototype.getRecurrPeriod = function () {
  return this.recurrPeriod;
};

ResMsgDTO.prototype.setRecurrPeriod = function (recurrPeriod) {
  this.recurrPeriod = recurrPeriod;
};

ResMsgDTO.prototype.getRecurrDay = function () {
  return this.recurrDay;
};

ResMsgDTO.prototype.setRecurrDay = function (recurrDay) {
  this.recurrDay = recurrDay;
};

ResMsgDTO.prototype.getNoOfRecurring = function () {
  return this.noOfRecurring;
};

ResMsgDTO.prototype.setNoOfRecurring = function (noOfRecurring) {
  this.noOfRecurring = noOfRecurring;
};

ResMsgDTO.prototype.getResponseUrl = function () {
  return this.responseUrl;
};

ResMsgDTO.prototype.setResponseUrl = function (responseUrl) {
  this.responseUrl = responseUrl;
};

// ResMsgDTO.prototype.getAddField1 = function () {
//   return this.addField1;
// };

// ResMsgDTO.prototype.setAddField1 = function (addField1) {
//   this.addField1 = addField1;
// };
ResMsgDTO.prototype.getName = function () {
  return this.name;
};

ResMsgDTO.prototype.setName = function (name) {
  this.name = name;
};

// ResMsgDTO.prototype.getAddField2 = function () {
//   return this.addField2;
// };

// ResMsgDTO.prototype.setAddField2 = function (addField2) {
//   this.addField2 = addField2;
// };
ResMsgDTO.prototype.getEmail = function () {
  return this.email;
};

ResMsgDTO.prototype.setEmail = function (email) {
  this.email = email;
};

// ResMsgDTO.prototype.getAddField3 = function () {
//   return this.addField3;
// };

// ResMsgDTO.prototype.setAddField3 = function (addField3) {
//   this.addField3 = addField3;
// };

// ResMsgDTO.prototype.getAddField4 = function () {
//   return this.addField4;
// };

// ResMsgDTO.prototype.setAddField4 = function (addField4) {
//   this.addField4 = addField4;
// };

// ResMsgDTO.prototype.getAddField5 = function () {
//   return this.addField5;
// };

// ResMsgDTO.prototype.setAddField5 = function (addField5) {
//   this.addField5 = addField5;
// };
ResMsgDTO.prototype.getContactNumber = function () {
  return this.contactNumber;
};

ResMsgDTO.prototype.setContactNumber = function (contactNumber) {
  this.contactNumber = contactNumber;
};

ResMsgDTO.prototype.getSpecialRequirement = function () {
  return this.specialRequirement;
};

ResMsgDTO.prototype.setSpecialRequirement = function (specialRequirement) {
  this.specialRequirement = specialRequirement;
};

ResMsgDTO.prototype.getRoomType = function () {
  return this.roomType;
};

ResMsgDTO.prototype.setRoomType = function (roomType) {
  this.roomType = roomType;
};

// ResMsgDTO.prototype.getAddField6 = function () {
//   return this.addField6;
// };

// ResMsgDTO.prototype.setAddField6 = function (addField6) {
//   this.addField6 = addField6;
// };

// ResMsgDTO.prototype.getAddField7 = function () {
//   return this.addField7;
// };

// ResMsgDTO.prototype.setAddField7 = function (addField7) {
//   this.addField7 = addField7;
// };

// ResMsgDTO.prototype.getAddField8 = function () {
//   return this.addField8;
// };

// ResMsgDTO.prototype.setAddField8 = function (addField8) {
//   this.addField8 = addField8;
// };

ResMsgDTO.prototype.getCheckinDate = function () {
  return this.checkinDate;
};

ResMsgDTO.prototype.setCheckinDate = function (checkinDate) {
  this.checkinDate = checkinDate;
};

ResMsgDTO.prototype.getCheckoutDate = function () {
  return this.checkoutDate;
};

ResMsgDTO.prototype.setCheckoutDate = function (checkoutDate) {
  this.checkoutDate = checkoutDate;
};

ResMsgDTO.prototype.getAdultNumber = function () {
  return this.adultNumber;
};

ResMsgDTO.prototype.setAdultNumber = function (adultNumber) {
  this.adultNumber = adultNumber;
};

// ResMsgDTO.prototype.getAddField9 = function () {
//   return this.addField9;
// };

// ResMsgDTO.prototype.setAddField9 = function (addField9) {
//   this.addField9 = addField9;
// };

// ResMsgDTO.prototype.getAddField10 = function () {
//   return this.addField10;
// };

// ResMsgDTO.prototype.setAddField10 = function (addField10) {
//   this.addField10 = addField10;
// };

ResMsgDTO.prototype.getChildMinorNumber = function () {
  return this.childMinorNumber;
};

ResMsgDTO.prototype.setChildMinorNumber = function (childMinorNumber) {
  this.childMinorNumber = childMinorNumber;
};

ResMsgDTO.prototype.getChildNumber = function () {
  return this.childNumber;
};

ResMsgDTO.prototype.setChildNumber = function (childNumber) {
  this.childNumber = childNumber;
};

ResMsgDTO.prototype.getTotalRoomTypes = function () {
  return this.totalRoomTypes;
};

ResMsgDTO.prototype.setTotalRoomTypes = function (totalRoomTypes) {
  this.totalRoomTypes = totalRoomTypes;
};
ResMsgDTO.prototype.getTotalRooms = function () {
  return this.totalRooms;
};

ResMsgDTO.prototype.setTotalRooms = function (totalRooms) {
  this.totalRooms = totalRooms;
};

ResMsgDTO.prototype.getReqMsg = function () {
  return this.reqMsg;
};

ResMsgDTO.prototype.setReqMsg = function (reqMsg) {
  this.reqMsg = reqMsg;
};

ResMsgDTO.prototype.getStatusDesc = function () {
  return this.statusDesc;
};

ResMsgDTO.prototype.setStatusDesc = function (statusDesc) {
  this.statusDesc = statusDesc;
};

ResMsgDTO.prototype.getEnckey = function () {
  return this.enckey;
};

ResMsgDTO.prototype.setEnckey = function (enckey) {
  this.enckey = enckey;
};

ResMsgDTO.prototype.getShippingCharges = function () {
  return this.shippingCharges;
};

ResMsgDTO.prototype.setShippingCharges = function (shippingCharges) {
  this.shippingCharges = shippingCharges;
};

ResMsgDTO.prototype.getTaxAmount = function () {
  return this.taxAmount;
};

ResMsgDTO.prototype.setTaxAmount = function (taxAmount) {
  this.taxAmount = taxAmount;
};

ResMsgDTO.prototype.getGrossTrnAmt = function () {
  return this.grossTrnAmt;
};

ResMsgDTO.prototype.setGrossTrnAmt = function (grossTrnAmt) {
  this.grossTrnAmt = grossTrnAmt;
};

ResMsgDTO.prototype.getPayTypeCode = function () {
  return this.payTypeCode;
};

ResMsgDTO.prototype.setPayTypeCode = function (payTypeCode) {
  this.payTypeCode = payTypeCode;
};

ResMsgDTO.prototype.getCardNumber = function () {
  return this.cardNumber;
};

ResMsgDTO.prototype.setCardNumber = function (cardNumber) {
  this.cardNumber = cardNumber;
};

ResMsgDTO.prototype.getExpiryDate = function () {
  return this.expiryDate;
};

ResMsgDTO.prototype.setExpiryDate = function (expiryDate) {
  this.expiryDate = expiryDate;
};

ResMsgDTO.prototype.getNameOnCard = function () {
  return this.nameOnCard;
};

ResMsgDTO.prototype.setNameOnCard = function (nameOnCard) {
  this.nameOnCard = nameOnCard;
};

ResMsgDTO.prototype.getCvv = function () {
  return this.cvv;
};

ResMsgDTO.prototype.setCvv = function (cvv) {
  this.cvv = cvv;
};

ResMsgDTO.prototype.getPgMeTrnRefNo = function () {
  return this.pgMeTrnRefNo;
};

ResMsgDTO.prototype.setPgMeTrnRefNo = function (pgMeTrnRefNo) {
  this.pgMeTrnRefNo = pgMeTrnRefNo;
};

ResMsgDTO.prototype.getCancelRefundFlag = function () {
  return this.cancelRefundFlag;
};

ResMsgDTO.prototype.setCancelRefundFlag = function (cancelRefundFlag) {
  this.cancelRefundFlag = cancelRefundFlag;
};

ResMsgDTO.prototype.getRefundAmt = function () {
  return this.refundAmt;
};

ResMsgDTO.prototype.setRefundAmt = function (refundAmt) {
  this.refundAmt = refundAmt;
};

ResMsgDTO.prototype.getNetBankCode = function () {
  return this.netBankCode;
};

ResMsgDTO.prototype.setNetBankCode = function (netBankCode) {
  this.netBankCode = netBankCode;
};

module.exports = ResMsgDTO;
// }
