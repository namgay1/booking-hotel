function submitFormWithBookingData() {
  // Get the bookingData from wherever it's stored (sessionStorage in your case)
  const bookingData = JSON.parse(sessionStorage.getItem("bookingData"));

  // Adjust keys and values
  bookingData.orderId = bookingData.reservationID;
  delete bookingData.reservationID;

  bookingData.trnAmt = bookingData.totalAmount;
  delete bookingData.totalAmount;

  // Remove the bookingData property
  delete bookingData.bookingData;

  // Convert the adjusted bookingData object to a formatted string
  let formattedData = "";
  for (const key in bookingData) {
    formattedData += `${key}: ${bookingData[key]}\n`;
  }

  // Remove the trailing newline character
  formattedData = formattedData.trim();

  // Set the value of the hidden input field
  document.getElementById("bookingDataInput").value = formattedData;
  // Remove the bookingData property from the bookingData object
  // delete bookingData.bookingData;
  // Submit the form
  document.getElementById("internationalPaymentForm").submit();
}
